import { NewsTypeEnum } from "../types"

export const newsTypeCodeObj = {
  [NewsTypeEnum.HEADLINE]: 'T1348647853363',
  [NewsTypeEnum.CHOICE]: 'T1467284926140',
  [NewsTypeEnum.ENTERTAINMENT]:'T1348648517839',
  [NewsTypeEnum.SPORTS]:'T1348649079062'
}

export const newsTypeUrlObj = {
  [NewsTypeEnum.HEADLINE]: 'http://c.m.163.com/nc/article/headline/T1348647853363',
  [NewsTypeEnum.CHOICE]: 'http://c.3g.163.com/nc/article/list/T1467284926140',
  [NewsTypeEnum.ENTERTAINMENT]: 'http://c.3g.163.com/nc/article/list/T1348648517839',
  [NewsTypeEnum.SPORTS]: 'http://c.3g.163.com/nc/article/list/T1348649079062',
}