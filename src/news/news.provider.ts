import { News } from './news.mysql.entity';

export const NewsProviders = [
  {
    provide: 'NEWS_REPOSITORY',
    useFactory: async (AppDataSource) => await AppDataSource.getRepository(News),
    inject: ['MYSQL_DATA_SOURCE'],
  }
];