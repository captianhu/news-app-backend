import { DatabaseModule } from '@/common/database/database.module';
import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { NewsProviders } from './news.provider';
import { NewsService } from './news.service';

@Module({
  imports: [
    DatabaseModule
  ],
  controllers: [NewsController],
  providers: [...NewsProviders,NewsService],
  exports: [NewsService, DatabaseModule],
})
export class NewsModule { }