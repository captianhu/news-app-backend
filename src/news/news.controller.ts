import {
  Controller,
  Get,
  Query,
  Version,
  VERSION_NEUTRAL,
} from '@nestjs/common';
import { NewsService } from './news.service';
import { ConfigService } from '@nestjs/config';
import { FindNewsDto } from './dto/find_news.dto';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { DetailNewsDto } from './dto/detail_news.dto';

@Controller({
  path: 'news',
  version: '1',
})
export class NewsController {
  constructor(
    private readonly newsService: NewsService,
    private readonly configService: ConfigService
  ) { }

  @ApiOperation({ summary: '新闻列表', description: '获取新闻列表结果' })
  @Get('list')
  @Version([VERSION_NEUTRAL])
  @ApiResponse({                                      
    status: 200, description: '返回新闻列表结果'
  })
  getNews(@Query() params:FindNewsDto) {
    return this.newsService.getNews(params);
  }

  @ApiOperation({ summary: '新闻详情', description: '获取新闻详情' })
  @Get('detail')
  @Version([VERSION_NEUTRAL])
  @ApiResponse({                                      // 处理响应
    status: 200, description: '返回新闻详情'
  })
  getNewsDetail(@Query() params:DetailNewsDto){
    return this.newsService.getNewsDetail(params.id)
  }
}
