export enum NewsTypeEnum {
  HEADLINE = 'headline',
  CHOICE = 'choice',
  ENTERTAINMENT = 'entertainment',
  SPORTS = 'sports',
}
