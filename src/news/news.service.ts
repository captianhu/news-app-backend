import { methodV } from '@/utils/request';
import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { newsTypeCodeObj, newsTypeUrlObj } from './constants';
import { FindNewsDto } from './dto/find_news.dto';
import { News } from './news.mysql.entity';

@Injectable()
export class NewsService {
  constructor( 
    @Inject('NEWS_REPOSITORY')
    private readonly newsRepository: Repository<News>
  ){}
  async getNews(params: FindNewsDto) {
    const { data } = await methodV({
      url: `${newsTypeUrlObj[params.type]}/${(params.page-1)*20}-20.html`,
      method: 'get',
    })
    return data[newsTypeCodeObj[params.type]];
  }
  async getNewsDetail(id:string){
    const { data } = await methodV({
      url: `https://v2.alapi.cn/api/new/detail`,
      method: 'post',
      params:{
        docid:id,
        token:'SzNhIt3GZnO3kpPz'
      }
    })
    let originalData = data.data
    data.data.img?.forEach(item=>{
      originalData.body = originalData.body.replace(`${item.ref}`,`<img src="${item.src}"/>`)
    })
    return originalData;
  }
}
