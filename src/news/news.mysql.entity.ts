import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('news') // name 填入表名称，会自动创建这个表
export class News {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  docId: string;
  @Column()
  likeNum: number;
}