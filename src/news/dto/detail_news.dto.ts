import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class DetailNewsDto {
  @ApiProperty({       // 通过ApiProperty 定义每一个字段类型
    type: String,
    description:"输入新闻docId"
  })
  @IsString()
  id:string
}
