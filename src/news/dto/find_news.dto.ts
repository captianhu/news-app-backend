import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsString } from 'class-validator';
import { NewsTypeEnum } from '../types';

export class FindNewsDto {
  @ApiProperty({       // 通过ApiProperty 定义每一个字段类型
    type: String,
    description:"可输入类型为'headline' | 'choice' | 'entertainment' | 'sports'"
  })
  @IsEnum(NewsTypeEnum)
  type:string
  @ApiProperty({       // 通过ApiProperty 定义每一个字段类型
    type: Number,    // 需输入数字，默认值是字符串
    minimum: 1,      // 最小值是1
    default: 1,      // 默认值是1
  })
  @IsString()
  page:number
}
