import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from '../user/user.mysql.entity';
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    // LocalStrategy默认只接收username和password，传入这些字段修改默认值
    super({
      usernameField: 'userName',
      passwordField: 'password',
    });
    this.authService = authService;
  }

  async validate(username: string, password: string): Promise<{userInfo: Omit<User,'password'>}> {
    const user = await this.authService.validate(username, password);
    if (!user) {
      throw new HttpException(
        { message: '授权失败', error: '请检查用户名和密码是否正确' },
        HttpStatus.BAD_REQUEST,
      );
    }
    return user
  }
}