import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { jwtContants } from './jwt.constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromHeader('token'),
      ignoreExpiration: false,  //是否忽略过期token
      secretOrKey: jwtContants.secret,
    });
  }
	// 使用UseGuards(AuthGuard('jwt')) 时，会自动进入此处验证
  async validate(payload: any) {
    return payload
  }
}