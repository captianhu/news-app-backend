import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user/user.mysql.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService,  private readonly jwtService: JwtService) {
    this.userService = userService;
    this.jwtService = jwtService;
  }

  /**
   * validate user name and password
   * @param username
   * @param password
   */
   async validate(username: string, password: string): Promise<{userInfo: Omit<User,'password'>}> {
    const user = await this.userService.find(username);
    if(user){
      const compareResult = await bcrypt.compare(password,user.password)
      if(compareResult){
        const { password, ...userInfo } = user;
        return { userInfo };
      }
      return null
    }
    return null;
  }

  // 使用jwt加密用户的信息到access_token返回
  async createToken(user: any,): Promise<{ access_token: string }> {
    const { password, ...payload } = user
    return { access_token: this.jwtService.sign(payload) };
  }
}
