import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { ApiOperation } from '@nestjs/swagger';
import { LoginDto } from './dto/login.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuard('local')) // local验证 账号和密码是否包含指定字段
  @ApiOperation({ summary: '登陆', description: '执行登陆' })
  @Post('/login')
  async login(@Body() body: LoginDto) {
    const user = await this.authService.validate(body.userName, body.password);
    return this.authService.createToken(user);
  }
}