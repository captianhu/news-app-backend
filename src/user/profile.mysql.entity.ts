import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from 'typeorm';
import { User } from './user.mysql.entity';

@Entity('profile') // name 填入表名称，会自动创建这个表
export class Profile {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  gender: string;
  @Column()
  photo: string;
  @OneToOne(() => User, (user) => user.profile) // specify inverse side as a second parameter
  user: User
}