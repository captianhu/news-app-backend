import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.mysql.entity';
import { AuthGuard } from '@nestjs/passport';
import { RegisterDto } from './dto/register.dto';
import { ApiOperation, ApiPropertyOptional } from '@nestjs/swagger';
import { ProfileDto } from './dto/profile.dto';
import { CollectionDto } from './dto/collection.dto';
import { OperateCollectionDto } from './dto/operateCollection.dto';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService
  ) {
    this.userService = userService;
  }
  @ApiOperation({ summary: '获取所有用户信息列表', description: '获取所有用户信息列表' })
  @UseGuards(AuthGuard('jwt'))
  @Get()
  async list(@Req() request): Promise<Array<Omit<User,'password'>>> {
    return this.userService.listAll();
  }
  @ApiOperation({ summary: '注册', description: '执行注册' })
  @Post('register')
  async create(@Body() body:RegisterDto) {
    return this.userService.create(body);
  }
  @ApiOperation({ summary: '查看用户资料', description: '编辑用户资料' })
  @UseGuards(AuthGuard('jwt'))
  @Post('/profile/get')
  async getProfile(@Req() request) {
    return this.userService.getProfile(request.user.id);
  }
  @ApiOperation({ summary: '编辑用户资料', description: '编辑用户资料' })
  @UseGuards(AuthGuard('jwt'))
  @Post('/profile/edit')
  async editProfile(@Req() request,@Body() body:ProfileDto) {
    return this.userService.editProfile(request.user.id,body);
  }
  @ApiOperation({ summary: '添加收藏', description: '添加收藏' })
  @UseGuards(AuthGuard('jwt'))
  @Post('/collection/add')
  async addCollection(@Req() request,@Body() body:CollectionDto) {
    return this.userService.createCollections(request.user.username,body);
  }
  @ApiOperation({ summary: '查看收藏', description: '查看收藏' })
  @UseGuards(AuthGuard('jwt'))
  @Post('/collection/list')
  async getCollection(@Req() request) {
    return this.userService.listAllCollections(request.user.username);
  }
  @ApiOperation({ summary: '判断当前文章是否被收藏', description: '判断当前文章是否被收藏' })
  @UseGuards(AuthGuard('jwt'))
  @Post('/collection/has')
  async hasCollection(@Req() request,@Body() body:OperateCollectionDto) {
    return this.userService.hasCollection(request.user.username,body.docId);
  }
  @ApiOperation({ summary: '删除收藏', description: '删除收藏' })
  @UseGuards(AuthGuard('jwt'))
  @Post('/collection/delete')
  @ApiPropertyOptional({
    name:'id',
    required: true,
    description: '新闻id'
  })
  async removeCollection(@Req() request,@Body() body:OperateCollectionDto) {
    return this.userService.removeCollections(request.user.username,body);
  }
}
