import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { BeforeInsert } from 'typeorm';
import { Profile } from './profile.mysql.entity';
import { Collect } from './collect.mysql.entity';

@Entity('user') // name 填入表名称，会自动创建这个表
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  username: string;
  @Column()
  password: string;
  @OneToOne(() => Profile, (profile) => profile.user)
  @JoinColumn()
  profile: Profile
  @OneToMany(() => Collect, (collect) => collect.user)
  collection: Collect[]
  @BeforeInsert()
  private async hashPassword() {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
  }
}