import { HttpException, HttpStatus, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { ProfileDto } from './dto/profile.dto';
import { RegisterDto } from './dto/register.dto';
import { Profile } from './profile.mysql.entity';
import { User } from './user.mysql.entity';
import { Collect } from './collect.mysql.entity'
import { CollectionDto } from './dto/collection.dto';
import { OperateCollectionDto } from './dto/operateCollection.dto';

@Injectable()
export class UserService {
  constructor(
    @Inject('USER_REPOSITORY')
    private readonly userRepository: Repository<User>,
    @Inject('PROFILE_REPOSITORY')
    private readonly profileRepository: Repository<Profile>,
    @Inject('COLLECT_REPOSITORY')
    private readonly collectRepository: Repository<Collect>,
  ) {}
  /**
   * find user by username
   * @param username
   */
  async find(username: string): Promise<User> {
    const user = this.userRepository.findOneBy({
      username
    });
    if (user) return user;
    else return null;
  }

  /**
   * list all users
   */
  async listAll(): Promise<Array<Omit<User,'password'>>> {
    const allUser = await this.userRepository.find()
    return allUser.map((user) => {
      const { password, ...info } = user;
      return info;
    });
  }

  /**
   * list all collections
   */
  async listAllCollections(username:string): Promise<Array<Collect>> {
    const currentUser = await this.userRepository.findOne({
      where:{
        username:username
      },
      relations: {
        collection: true,
      },
    })
    return currentUser.collection
  }

  async createCollections(username:string,collection:CollectionDto){
    const existCollectionData = await this.preloadCollectionByName(username)
    const collectionData = await this.collectRepository.create(collection)
    await this.collectRepository.save(collectionData)
    let currentUser = await this.userRepository.findOne({
      where:{
        username:username
      },
      relations: {
        collection: true,
      },
    })
    currentUser.collection = [...existCollectionData,collectionData]
    return await this.userRepository.save(currentUser)
  }

  async hasCollection(name:string,id:string){
    const currentUser = await this.userRepository.findOne({
      where:{
        username:name
      },
      relations: {
        collection: true,
      },
    })
    return currentUser.collection.findIndex(item=>item.docId === id) !== -1
  }

  private async preloadCollectionByName(name: string): Promise<Collect[]> {
    const currentUser = await this.userRepository.findOne({
      where:{
        username:name
      },
      relations: {
        collection: true,
      },
    })
    return currentUser.collection;
  }

  async removeCollections(userName:string,body:OperateCollectionDto){
    let collectionData = await this.listAllCollections(userName)
    let waitForCollectionData = collectionData.find(item=>item.docId === body.docId)
    return await this.collectRepository.delete(waitForCollectionData)
  }

  async create(user:RegisterDto) {
    const exist = await this.find(user.userName)
    if(!exist){
      const profileData = this.profileRepository.create({
        name:'',
        gender:'',
        photo:''
      })
      const profileSavedData = await this.profileRepository.save(profileData)
      const userData = this.userRepository.create({
        username:user.userName,
        password:user.password,
        profile:profileSavedData
      })
      return this.userRepository.save(userData)
    }
    throw new HttpException(
      { message: '注册失败', error: '用户已存在' },
      HttpStatus.BAD_REQUEST,
    );
  }

  async getProfile(id:number){
    const user = await this.userRepository.findOne({
      where:{
        id:id
      },
      relations: {
        profile: true,
      },
    })
    return user.profile
  }
  async editProfile(id:number,body:Omit<ProfileDto,'id'>){
    const user = await this.userRepository.findOne({
      where:{
        id:id
      },
      relations: {
        profile: true,
      },
    })
    if(user){
      let profileData = await this.preloadProfileById(user.profile.id)
      profileData.gender = body.gender
      profileData.name = body.name
      profileData.photo = body.photo
      return this.profileRepository.save(profileData)
    }else{
      throw new NotFoundException('未找到相关用户')
    }
  }

  private async preloadProfileById(id: number) {
    const existingProfile = await this.profileRepository.findOneBy({ id });
    if (existingProfile) {
      return existingProfile;
    }
    return this.profileRepository.create({ id,name:'', gender:'', photo:'' });
  }
}