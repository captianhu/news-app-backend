import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.mysql.entity';

@Entity('collect') // name 填入表名称，会自动创建这个表
export class Collect {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  docId: string;
  @Column()
  title: string;
  @Column()
  photo: string;
  @Column()  
  source: string
  @ManyToOne(() => User, (user) => user.collection)
  public user: User
}