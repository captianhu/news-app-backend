import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserProviders } from './user.provider';

import { UserController } from './user.controller';
import { DatabaseModule } from '@/common/database/database.module';
@Module({
  imports: [
    DatabaseModule
  ],
  providers: [...UserProviders,UserService],
  controllers: [UserController],
  exports: [UserService, DatabaseModule],
})
export class UserModule {}
