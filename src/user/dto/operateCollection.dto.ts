import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class OperateCollectionDto {
  @IsNotEmpty({ message: 'id不能为空' })
  @ApiPropertyOptional({
    required: true,
    description: '新闻id'
  })
  public docId: string;
}