import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { ProfileDto } from './profile.dto';

export class RegisterDto {
  @IsNotEmpty({ message: '密码不能为空' })
  @MaxLength(18, { message: '密码最长为18位数' })
  @MinLength(8, { message: '密码最少8位数' })
  @ApiPropertyOptional({
    required: true,
    description: '密码',
    default: '12345678',
  })
  public password: string;

  @IsNotEmpty({ message: '用户名不能为空' })
  @IsString({ message: '用户名必须为字符串' })
  @ApiPropertyOptional({
    required: true,
    description: '用户名',
    default: 'admin',
  })
  public readonly userName: string;
  public profile: ProfileDto
}