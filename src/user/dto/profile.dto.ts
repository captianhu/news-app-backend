import { ApiPropertyOptional } from '@nestjs/swagger';

export class ProfileDto {
  @ApiPropertyOptional({
    required: true,
    description: '用户id',
    default: 1
  })
  public id: number;
  @ApiPropertyOptional({
    required: false,
    description: '姓名',
    default: '测试姓名'
  })
  public name: string;
  @ApiPropertyOptional({
    required: false,
    description: '性别',
    default: '男'
  })
  public gender: string;
  @ApiPropertyOptional({
    required: false,
    description: '头像',
    default:'https://element-plus.org/images/figure-1.png'
  })
  public photo: string;
}