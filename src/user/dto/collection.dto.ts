import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { User } from '../user.mysql.entity';

export class CollectionDto {
  @IsNotEmpty({ message: 'id不能为空' })
  @ApiPropertyOptional({
    required: true,
    description: '新闻id'
  })
  public docId: string;
  @ApiPropertyOptional({
    required: false,
    description: '新闻标题',
  })
  public title: string;
  @ApiPropertyOptional({
    required: false,
    description: '新闻封面',
  })
  public photo: string;
  @ApiPropertyOptional({
    required: false,
    description: '来源',
  })
  public source: string;
}