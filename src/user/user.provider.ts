import { User } from './user.mysql.entity';
import { Profile } from './profile.mysql.entity';
import { Collect } from './collect.mysql.entity';

export const UserProviders = [
  {
    provide: 'USER_REPOSITORY',
    useFactory: async (AppDataSource) => await AppDataSource.getRepository(User),
    inject: ['MYSQL_DATA_SOURCE'],
  },
  {
    provide: 'PROFILE_REPOSITORY',
    useFactory: async (AppDataSource) => await AppDataSource.getRepository(Profile),
    inject: ['MYSQL_DATA_SOURCE'],
  },
  {
    provide: 'COLLECT_REPOSITORY',
    useFactory: async (AppDataSource) => await AppDataSource.getRepository(Collect),
    inject: ['MYSQL_DATA_SOURCE'],
  }
];