import { Controller, FileTypeValidator, Get, MaxFileSizeValidator, ParseFilePipe, Post, Req, UseInterceptors } from '@nestjs/common';
import { ApiConsumes, ApiOperation } from '@nestjs/swagger';
import { AppService } from './app.service';
import { join } from 'path';
import fs from 'fs';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @ApiOperation({ summary: '上传', description: '上传接口' })
  @ApiConsumes('multipart/form-data')
  @Post('upload')
  async upload(@Req() req) {
    const multipart = req.body
    let time = new Date().getTime();
    let filename = time+"_"+multipart['file'][0].filename;
    let data = multipart['file'][0].data
    //文件上传路径
    let path = join(__dirname, './upload/single');
    const writerStream = fs.createWriteStream(
        path + filename
    );
    writerStream.write(data);
    await writerStream.end();
    return {
        code: 200,
        message: "success",
        data:{
          url: "/upload/" + filename
        } //此处为返回给前端的文件路径，前端可以通过此URL访问到文件
    }
  }
}
