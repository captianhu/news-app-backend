import { DataSource } from 'typeorm';
import { getConfig } from 'src/utils/index'

const path = require('path')
// 设置数据库类型
const { MYSQL_CONFIG } = getConfig()

const DATABASE_CONFIG = {
  ...MYSQL_CONFIG,
  entities:[path.join(__dirname, `../../**/*.mysql.entity{.ts,.js}`)]
}

const MYSQL_DATA_SOURCE = new DataSource(DATABASE_CONFIG)

// 数据库注入
export const DatabaseProviders = [
  {
    provide: 'MYSQL_DATA_SOURCE',
    useFactory: async () => {
      await MYSQL_DATA_SOURCE.initialize()
      return MYSQL_DATA_SOURCE
    }
  }
];